import pandas as pd
import numpy as np
from itertools import chain
from bs4 import BeautifulSoup
import matplotlib.pylab as plt
from sklearn.decomposition import PCA
plt.style.use('ggplot')

# def test(xml_f, xml_type):
#     f = open(xml_f, 'r')
#     soup = BeautifulSoup(f)
#     df = pd.DataFrame()
#     other_types = set([])
#     for j in soup.healthdata:
#         try:
#             if j['type'] == xml_type:
#                 df = df.append(pd.DataFrame.from_dict(j.attrs, orient = 'index').T)
#             else:
#                 other_types.add(j['type'])
#         except:
#             pass
#     df.startdate = pd.to_datetime(df.startdate)
#     df.enddate = pd.to_datetime(df.enddate)
#     df.value = df.value.astype(float)
#     return df, list(other_types)

# df, other_types = test('export.xml', 'HKQuantityTypeIdentifierStepCount')
df = pd.DataFrame.from_csv('Steps.csv')
print df.dtypes
pivoted = df.pivot_table(['Steps'], index =df.index.date, columns = df.index.hour, fill_value = 0)
print pivoted
# X = pivoted.values
# Xpca = PCA(0.90).fit_transform(X)
# print Xpca.shape
# total_steps = X.sum(1)
# plt.scatter(Xpca[:, 0], Xpca[:, 1], c=total_steps,
#             cmap='cubehelix')
# plt.colorbar(label='total steps')
# plt.show()
# from sklearn.mixture import GMM
# gmm = GMM(2, covariance_type='full', random_state=0)
# gmm.fit(Xpca)
# cluster_label = gmm.predict(Xpca)
# # plt.scatter(Xpca[:, 0], Xpca[:, 1], c=cluster_label)
# # plt.show()
# pivoted['Cluster'] = cluster_label
# df = df.join(pivoted['Cluster'], on=df.index.date)
# print df
# dayofweek = pd.to_datetime(pivoted.index).dayofweek
# plt.scatter(Xpca[:, 0], Xpca[:, 1], c=dayofweek,
#             cmap=plt.cm.get_cmap('jet', 7))
# cb = plt.colorbar(ticks=range(7))
# cb.set_ticklabels(['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun'])
# plt.clim(-0.5, 6.5);
# plt.show()
# x = df.startdate.values
# y = df.value.values
# plt.scatter(x= x, y = y)
# plt.show()
# pd.DataFrame.hist(data, column = ['weekday'])
# plt.show()